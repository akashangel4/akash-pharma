import React from 'react';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';

export default function About() {
  return (
    <>
    <Header />
    <div className='container'>
    <h1>About Us page</h1>
    </div>
    <Footer />
    </>
  )
}
