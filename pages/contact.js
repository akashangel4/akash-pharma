import React from 'react';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';

export default function Contact() {
  return (
    <>
    <Header />
    <div className='container'>
    <h1>Contact page</h1>
    </div>
    <Footer />
    </>
  )
}
