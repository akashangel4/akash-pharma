import React from 'react';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';

export default function Home() {
  return (
    <>
    <Header />
    <div className='container'>
    <h1>Home page</h1>
    </div>
    <Footer />
    </>
  )
}
